
// set the dimensions and margins of the graph
var margin = {top: 30, right: -140, bottom: 30, left: 100},
  width = 450 - margin.left - margin.right,
  height = 450 - margin.top - margin.bottom;

// append the svg object to the body of the page
var svg_heatmap = d3.select("#heatmap")
.append("svg")
  .attr("width", width + margin.left - margin.right)
  .attr("height", height + margin.top + margin.bottom)
.append("g")
  .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");



var data = {};
data.melted = [{
    "geneID": "Gene 4",
    "condition": "Treatment 1",
    "value": 55.92
  },
  {
    "geneID": "Gene 4",
    "condition": "Treatment 10",
    "value": 44.26
  },
  {
    "geneID": "Gene 4",
    "condition": "Treatment 26",
    "value": 49.18
  },
  {
    "geneID": "Gene 4",
    "condition": "Treatment 73",
    "value": 34.8
  },
  {
    "geneID": "Gene 6",
    "condition": "Treatment 1",
    "value": 47.26
  },
  {
    "geneID": "Gene 6",
    "condition": "Treatment 10",
    "value": 45.08
  },
  {
    "geneID": "Gene 6",
    "condition": "Treatment 26",
    "value": 89.95
  },
  {
    "geneID": "Gene 6",
    "condition": "Treatment 73",
    "value": 48.29
  },
  {
    "geneID": "Gene 8",
    "condition": "Treatment 1",
    "value": 9.79
  },
  {
    "geneID": "Gene 8",
    "condition": "Treatment 10",
    "value": 10.12
  },
  {
    "geneID": "Gene 8",
    "condition": "Treatment 26",
    "value": 10.48
  },
  {
    "geneID": "Gene 8",
    "condition": "Treatment 73",
    "value": 14.73
  },
  {
    "geneID": "Gene 9",
    "condition": "Treatment 1",
    "value": 16.76
  },
  {
    "geneID": "Gene 9",
    "condition": "Treatment 10",
    "value": 18.15
  },
  {
    "geneID": "Gene 9",
    "condition": "Treatment 26",
    "value": 17.26
  },
  {
    "geneID": "Gene 9",
    "condition": "Treatment 73",
    "value": 16.59
  },
];
d3.csv("https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/heatmap_data.csv", function(data) {
console.log(data);
});

// Labels of row and columns
var myGroups = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"]
var myVars = ["v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8", "v9", "v10"]
var myGroups = ['Gene 4', 'Gene 6', 'Gene 8', 'Gene 9'];
var myVars  = ['PAG000001', 'PAG000002', 'PAG000003', 'PAG000004'];

// Build X scales and axis:
var x = d3.scaleBand()
  .range([ 0, width ])
  .domain(myGroups)
  .padding(0.01);
svg_heatmap.append("g")
  .attr("transform", "translate(0," + height + ")")
  .call(d3.axisBottom(x))

// Build X scales and axis:
var y = d3.scaleBand()
  .range([ height, 0 ])
  .domain(myVars)
  .padding(0.01);
svg_heatmap.append("g")
  .call(d3.axisLeft(y));

// Build color scale
var myColor = d3.scaleLinear()
  .range(["white", "#69b3a2"])
  .domain([1,100])

// Declare range
var min = d3.min(data.melted, function(d) {
    return d.value;
  }),
  max = d3.max(data.melted, function(d) {
    return d.value;
  });
console.log(min,max);
// Fill
var fills = colorbrewer.RdBu[6];
var myColor = d3.scaleLinear().domain(d3.range(0, 100, 1)).range(fills);
var myColor = d3.scaleLinear()
  .domain([-1, 0, 1])
  .range(["blue", "white", "red"]);

d3.tsv("./files/mtx.csv", function(data){
data.melted=data;
console.log(data);
  // create a tooltip
  var tooltip = d3.select("#heatmap")
    .append("div")
    .style("opacity", 0)
    .attr("class", "tooltip")
    .style("background-color", "white")
    .style("border", "solid")
    .style("border-width", "2px")
    .style("border-radius", "5px")
    .style("padding", "5px")

  // Three function that change the tooltip when user hover / move / leave a cell
  var mouseover = function(d) {
    tooltip.style("opacity", 1)
  }
  var mousemove = function(d) {
    tooltip
      .html("The exact value of<br>this cell is: " + d.value)
      .style("left", (d3.mouse(this)[0]+140) + "px")
      .style("top", (d3.mouse(this)[1]) + "px")
  }
  var mouseleave = function(d) {
    tooltip.style("opacity", 0)
  }

  // add the squares
  svg_heatmap.selectAll()
  
    .data(data.melted, function(d) {return d.gene+':'+d.PAG;})
    .enter()
    .append("rect")
      .attr("x", function(d) { return x(d.gene) })
      .attr("y", function(d) { return y(d.PAG) })
      .attr("width", x.bandwidth() )
      .attr("height", y.bandwidth() )
      .style("fill", function(d) { return myColor(d.value)} )
    .on("mouseover", mouseover)
    .on("mousemove", mousemove)
    .on("mouseleave", mouseleave)
});