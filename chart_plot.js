
//var ctx = document.getElementById("myChart");
//var myChart = new Chart(ctx, {
//  type: 'bar',
//  data: {
//    labels: ["FEX000354", "MOX004058", "MOX003727", "MAX000662", "1MOX004729"],
//    datasets: [{
//      data: [102, 202, 58.4, 0, 117],
//      lineTension: 0,
//      backgroundColor: 'transparent',
//      borderColor: '#007bff',
//      borderWidth: 4,
//      pointBackgroundColor: '#007bff'
//    }]
//  },
//  options: {
//    scales: {
//      yAxes: [{
//        ticks: {
//          beginAtZero: false
//        }
//      }]
//    },
//    legend: {
//      display: false,
//    }
//  }
//});


// set the dimensions and margins of the graph
var margin = {top: 20, right: 30, bottom: 40, left: 90},
    width = 460 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

// append the svg object to the body of the page
var svg = d3.select("#horizontal")
  .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");


 // Parse the Data
d3.tsv("./files/PAGER3.csv", function(data){
	console.log(data);
	data = data.sort(function(x, y){
	return d3.ascending(parseFloat(x.pvalue), parseFloat(y.pvalue));
	})
	data = data.slice(0, 10);
	console.log(data);
  // Add X axis
  var x = d3.scaleLinear()
    .domain([0, -Math.log(d3.min(data, d => d.pvalue))/Math.log(10)])
    .range([ 0, width]);
  svg.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x))
    .selectAll("text")
      .attr("transform", "translate(-10,0)rotate(-45)")
      .style("text-anchor", "end");

  // Y axis
  var y = d3.scaleBand()
    .range([ 0, height ])
    .domain(data.map(function(d) { return d.PAGID; }))
    .padding(.1);
  svg.append("g")
    .call(d3.axisLeft(y))

  var tooltip = d3.select("#horizontal")
    .append("svg")
    .style("opacity", 0)
    .attr("class", "tooltip")
    .style("background-color", "white")
    .style("border", "solid")
    .style("border-width", "1px")
    .style("border-radius", "5px")
    .style("padding", "10px")

  // Three function that change the tooltip when user hover / move / leave a cell
  var mouseover = function(d) {
    var subgroupName = d3.select(d.pvalue).datum().key;
    tooltip
        .html("subgroup: " + pvalue + "<br>" + "Value: " + pvalue)
        .style("opacity", 1)
  }
  var mousemove = function(d) {
    tooltip
      .style("left", (d3.mouse(this)[0]+90) + "px") // It is important to put the +90: other wise the tooltip is exactly where the point is an it creates a weird effect
      .style("top", (d3.mouse(this)[1]) + "px")
  }
  var mouseleave = function(d) {
    tooltip
      .style("opacity", 0)
  }

  //Bars
  svg.selectAll("myRect")
    .data(data)
    .enter()
    .append("rect")
    .attr("x", x(0) )
    .attr("y", function(d) { return y(d.PAGID); })
    .attr("width", function(d) { return x(-Math.log(d.pvalue)/Math.log(10)); })
    .attr("height", y.bandwidth() )
    .attr("fill", "#69b3a2")
  svg.append("text")	
    .attr("class", "x label")
    .attr("text-anchor", "end")
    .attr("x", width - 120)
    .attr("y", height+35)
    .text("-log10 p-value");

    // .attr("x", function(d) { return x(d.PAGID); })
    // .attr("y", function(d) { return y(d.Value); })
    // .attr("width", x.bandwidth())
    // .attr("height", function(d) { return height - y(d.Value); })
    // .attr("fill", "#69b3a2")

})